firegento-contentsync
=====================

Share content between Magento installations through the file system (i.e. via Git)

This Magento module allows exporting and importing of content (for example CMS pages and blocks)
to the file system.

Status: unstable
